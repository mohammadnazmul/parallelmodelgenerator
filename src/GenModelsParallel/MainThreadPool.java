/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GenModelsParallel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.classifiers.Classifier;
import weka.classifiers.bayes.BayesNet;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.misc.VFI;
import weka.classifiers.functions.LibSVM;
import weka.classifiers.functions.Logistic;
import weka.classifiers.functions.SGD;
import weka.classifiers.functions.SimpleLogistic;
import weka.classifiers.functions.VotedPerceptron;
import weka.classifiers.lazy.IBk;
import weka.classifiers.lazy.KStar;
import weka.classifiers.lazy.LWL;
import weka.classifiers.rules.DecisionTable;
import weka.classifiers.rules.JRip;
import weka.classifiers.rules.OneR;
import weka.classifiers.rules.PART;
import weka.classifiers.rules.ZeroR;
import weka.classifiers.trees.DecisionStump;
import weka.classifiers.trees.HoeffdingTree;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.REPTree;
import weka.classifiers.trees.RandomTree;
import weka.core.Instances;
import weka.core.Utils;
import weka.core.converters.ConverterUtils;

/**
 *
 * @author mohammad
 */
public class MainThreadPool {

    public static int numFolds = 10;
    public static Instances[] cvTrnData = new Instances[numFolds];
    public static Instances[] cvTstData = new Instances[numFolds];
    public static int numClassifier = 20;
    public static int numThread;
    public static Classifier fCls;
    public static boolean isDebug = true;
    public static Instances trainDataSet;

    public static String[] clsfNames = new String[]{
        "BayesNet", "DecisionStump", "DecisionTable", "IBk", "J48", 
        "JRip", "KStar", "LibSVM", "Logistic", "LWL", 
        "NaiveBayes", "OneR", "PART", "RandomTree",  "REPTree", 
        "SGD", "SimpleLogistic", "VFI", "VotedPerceptron", "ZeroR"
    };

        
    public static Classifier[] models = {
        new BayesNet(),   new DecisionStump(),    new DecisionTable(),    new IBk(),                new J48(),
        new JRip(),       new KStar(),            new LibSVM(),           new Logistic(),           new LWL(),        
        new NaiveBayes(), new OneR(),             new PART(),             new RandomTree(),         new REPTree(),    
        new SGD(),        new SimpleLogistic(),   new VFI(),              new VotedPerceptron(),  new ZeroR()
    };
    

    static String ouCvModelFolder, inCvDataFolder;

    public static void main(String[] args) {

        if (args.length == 0) {
            System.out.println("Usage: java -jar ParallelModelGenerator.jar <-options optValue>");
            System.out.println("\nAvailable Options:");
            System.out.println("-i = Input files PATH for CV Folds (pre-generated).");
            System.out.println("-t = Input training file.");
            System.out.println("-o = Output Model Save PATH for the Cross-Validation/FullModel.");
            System.out.println("\nBuild Date: 24-Nov-14");
        } else {
            boolean isFullModel = false;

            try {
                inCvDataFolder = Utils.getOption("i", args);
            } catch (Exception ex) {
                System.err.println("Error: Must Specify input path ( -i ) ");
                Logger.getLogger(MainThreadPool.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("-i: " + inCvDataFolder);
            try {
                ouCvModelFolder = Utils.getOption("o", args);
            } catch (Exception ex) {
                System.err.println("Error: Must Specify output path ( -o ) ");
                Logger.getLogger(MainThreadPool.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("-o: " + ouCvModelFolder);
            try {
                String trnFile = Utils.getOption("t", args);
                if (trnFile.length() != 0) {
                    isFullModel = true;

                    System.out.println("Reading Training Dataset");

                    trainDataSet = ConverterUtils.DataSource.read(trnFile);
                    trainDataSet.setClassIndex(trainDataSet.numAttributes() - 1);
                    GenerateAllModelsParallel();

                } else {
                    System.out.println("Loading CV Fold Datasets");

                    LoadCVFoldsData(inCvDataFolder);
                    GenerateCVModelsParallel();
                }
            } catch (Exception ex) {
                Logger.getLogger(MainThreadPool.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("\nDone!");
    }

    public static void GenerateCVModelsParallel() throws Exception {

        int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available CPU: " + processors);
        if (processors > 2) {
            processors = processors - 2;
        } else {
            processors = 1;
        }
        System.out.println("Using CPU: " + processors + "\n");
        System.out.println("#Classifier: " + models.length + "\n");

        TrainClassifierParallel paraModelGenerator = new TrainClassifierParallel();
        paraModelGenerator.setNumExecutionSlots(processors);
        //paraModelGenerator.setFoldNumber(0);
        paraModelGenerator.setPath(ouCvModelFolder);
        paraModelGenerator.buildCVModels();

        // Build all FAILED cv models serially
        int totFailed = paraModelGenerator.getNumFailed();
        System.out.println("Total Failed CV Model Building: " + totFailed);
        if (totFailed > 0) {
            if (isDebug == true) {
                System.out.println("Started: " + totFailed + " Failed Model Building");
            }
            ArrayList<String> failedModel = paraModelGenerator.getFailedClassifeirs();
            for (int f = 0; f < totFailed; f++) {
                // Split Classifeir Name & Fold no
                String currFM = failedModel.get(f);
                BuildFailedCVModel(currFM);
            } // endFor : totFailed
            if (isDebug == true) {
                System.out.println("Finished: " + totFailed + " Model Building");
            }
        } // endIF : totFailed
    }

    public static void LoadCVFoldsData(String cvFolder) throws IOException, Exception {
        for (int n = 0; n < numFolds; n++) {
            Instances train = ConverterUtils.DataSource.read((cvFolder + "train-fold-" + n + ".arff"));
            train.setClassIndex(train.numAttributes() - 1);
            System.out.println("Read:" + (cvFolder + "train-fold-" + n + ".arff"));
            cvTrnData[n] = train;

            Instances test = ConverterUtils.DataSource.read((cvFolder + "test-fold-" + n + ".arff"));
            test.setClassIndex(test.numAttributes() - 1);
            System.out.println("Read:" + (cvFolder + "test-fold-" + n + ".arff"));
            cvTstData[n] = test;
        }
    }

    public static void BuildFailedCVModel(String failedModel) throws Exception {
        String[] parts = failedModel.split("-");
        String clsName = parts[0];
        int foldNo = Integer.parseInt(parts[1]);

        // Find Classifier from model array
        for (int cl = 0; cl < models.length; cl++) {
            String thisCls = models[cl].getClass().getSimpleName();
            if (thisCls.equals(clsName)) {
                fCls = models[cl];
                break;
            }
        }

        if (isDebug == true) {
            System.out.println("Start: Building " + fCls.getClass().getSimpleName() + "-" + foldNo);
        }
        fCls.buildClassifier(cvTrnData[foldNo]);

        // serialize model
        String saveName = ouCvModelFolder + failedModel + ".model";
        weka.core.SerializationHelper.write(saveName, fCls);

        if (isDebug == true) {
            System.out.println("CV Model Saved: " + saveName);
        }
    }

    public static void GenerateAllModelsParallel() throws Exception {
        int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available CPU: " + processors);
        processors = processors - 2;
        if (processors > models.length) {
            processors = models.length;
        }

        System.out.println("Using CPU: " + processors + "\n");
        System.out.println("#Classifier: " + models.length + "\n");

        TrainClassifierParallel fullModelGenPar = new TrainClassifierParallel();
        fullModelGenPar.setNumExecutionSlots(processors);
        fullModelGenPar.setPath(ouCvModelFolder);
        fullModelGenPar.buildClassifiers(trainDataSet);

        // Build all FAILED cv models serially
        int totFailed = fullModelGenPar.getNumFailed();
        System.out.println("Failed: " + totFailed);
        if (totFailed > 0) {
            if (isDebug == true) {
                System.out.println("Started: " + totFailed + " Failed Model Building");
            }
            ArrayList<String> failedModel = fullModelGenPar.getFailedClassifeirs();
            for (int f = 0; f < totFailed; f++) {
                // Split Classifeir Name & Fold no
                String currFM = failedModel.get(f);
                BuildFailedModel(currFM);
            } // endFor : totFailed
            if (isDebug == true) {
                System.out.println("Finished: " + totFailed + " Model Building");
            }
        } // endIF : totFailed
    }

    public static void BuildFailedModel(String failedModel) throws Exception {
        String[] parts = failedModel.split("-");
        String clsName = parts[0];

        // Find Classifier from model array
        for (int cl = 0; cl < models.length; cl++) {
            String thisCls = models[cl].getClass().getSimpleName();
            /*if (isDebug == true) {
             System.out.println("Classifier:" + thisCls + "\t" + clsName);
             }*/
            if (thisCls.equals(clsName)) {
                fCls = models[cl];
                break;
            }
        }

        if (isDebug == true) {
            System.out.println("Start: Building " + fCls.getClass().getSimpleName());
        }
        fCls.buildClassifier(trainDataSet);
        //eval.evaluateModel(fCls, TestDataSet);
        String modPath = ouCvModelFolder + failedModel + ".model";
        // serialize model
        weka.core.SerializationHelper.write(modPath, fCls);

        if (isDebug == true) {
            System.out.println("Full Model Saved: " + modPath);
            //System.out.println(eval.toMatrixString("=== Confusion matrix for fold " + (fold + 1) + "/" + numFolds + " ===\n"));
            //System.out.println("MCC: " + eval.weightedMatthewsCorrelation());
        }
    }
}
