package GenModelsParallel;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import weka.classifiers.Classifier;
import weka.core.Instances;
import weka.core.Utils;

import static GenModelsParallel.MainThreadPool.models;
import static GenModelsParallel.MainThreadPool.cvTrnData;
import static GenModelsParallel.MainThreadPool.cvTstData;
import static GenModelsParallel.MainThreadPool.isDebug;
import static GenModelsParallel.MainThreadPool.numFolds;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mohammad
 */
public class TrainClassifierParallel {

    /**
     * For serialization
     */
    private static final long serialVersionUID = 728109028953726626L;
    /**
     * The number of threads to have executing at any one time
     */
    protected int m_numExecutionSlots = 8;
    /**
     * Pool of threads to train models with
     */
    protected transient ThreadPoolExecutor m_executorPool;
    /**
     * The number of classifiers completed so far
     */
    protected int m_completed;
    /**
     * The number of classifiers that experienced a failure of some sort during
     * construction
     */
    protected String savePath;
    protected int m_failed;
    protected int m_fold;
    protected ArrayList<String> failed = new ArrayList<String>();

    /**
     * Parses a given list of options. Valid options are:
     * <p>
     *
     * -Z num <br>
     * Set the number of execution slots to use (default 1 - i.e. no
     * parallelism).
     * <p>
     *
     * Options after -- are passed to the designated classifier.<p>
     *
     * @param options the list of options as an array of strings
     * @exception Exception if an option is not supported
     */
    public void setOptions(String[] options) throws Exception {
        String iterations = Utils.getOption("num-slots", options);
        if (iterations.length() != 0) {
            setNumExecutionSlots(Integer.parseInt(iterations));
        } else {
            setNumExecutionSlots(1);
        }
    }

    /**
     * Set the number of execution slots (threads) to use for building the
     * members of the ensemble.
     *
     * @param numSlots the number of slots to use.
     */
    public void setNumExecutionSlots(int numSlots) {
        m_numExecutionSlots = numSlots;
    }

    public void setFoldNumber(int numFold) {
        m_fold = numFold;
    }

    public void setPath(String path) {
        savePath = path;
    }

    /**
     * Get the number of execution slots (threads) to use for building the
     * members of the ensemble.
     *
     * @return the number of slots to use
     */
    public int getNumExecutionSlots() {
        return m_numExecutionSlots;
    }

    public int getNumFailed() {
        return m_failed;
    }

    public ArrayList<String> getFailedClassifeirs() {
        return failed;
    }

    public void initFailedModels(ArrayList<String> failedModel) throws Exception {
        if (m_numExecutionSlots < 1) {
            throw new Exception("Number of execution slots needed to be >= 1!");
        }

        if (m_numExecutionSlots > 1) {
            if (isDebug == true) {
                System.out.println("Starting executor pool with " + m_numExecutionSlots
                        + " slots...");
            }
            startExecutorPool();
        }
        m_completed = 0;
        m_failed = 0;

        if (isDebug == true) {
            System.out.println("Started: " + failedModel.size() + " Failed Model Building");
        }
        int totFailed = failedModel.size();
        for (int f = 0; f < totFailed; f++) {
            // Split Classifeir Name & Fold no
            String currFM = failedModel.get(f);

        } // endFor : totFailed
        if (isDebug == true) {
            System.out.println("Finished: " + totFailed + " Model Building");
        }

    }

    /**
     * Stump method for building the classifiers
     *
     * @param data the training data to be used for generating the ensemble
     * @exception Exception if the classifier could not be built successfully
     */
    public void buildClassifier(Instances data) throws Exception {

        if (m_numExecutionSlots < 1) {
            throw new Exception("Number of execution slots needed to be >= 1!");
        }

        if (m_numExecutionSlots > 1) {
            if (isDebug == true) {
                System.out.println("Starting executor pool with " + m_numExecutionSlots
                        + " slots...");
            }
            startExecutorPool();
        }
        m_completed = 0;
        m_failed = 0;
    }

    /**
     * Start the pool of execution threads
     */
    protected void startExecutorPool() {
        if (m_executorPool != null) {
            m_executorPool.shutdownNow();
        }

        m_executorPool = new ThreadPoolExecutor(m_numExecutionSlots, m_numExecutionSlots,
                120, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
    }

    private synchronized void block(boolean tf) {
        if (tf) {
            try {
                if (m_numExecutionSlots > 1 && m_completed + m_failed < models.length) {
                    wait();
                }
            } catch (InterruptedException ex) {
            }
        } else {
            notifyAll();
        }
    }

    /**
     * Does the actual construction of the ensemble
     *
     * @throws Exception if something goes wrong during the training process
     */
    protected synchronized void buildClassifiers(final Instances data) throws Exception {
        if (isDebug == true) {
            System.out.println("Starting executor pool with " + m_numExecutionSlots
                    + " slots...");
        }
        startExecutorPool();
        m_completed = 0;
        m_failed = 0;

        for (int i = 0; i < models.length; i++) {
            if (m_numExecutionSlots > 1) {

                final Classifier currentClassifier = models[i];
                final int iteration = i;
                Runnable newTask = new Runnable() {
                    public void run() {
                        try {
                            if (isDebug == true) {
                                System.out.println("Training classifier (" + (iteration + 1) + ")");
                            }
                            // serialize model
                            String modelName = savePath + currentClassifier.getClass().getSimpleName() + ".model";
                            File f = new File(modelName);
                            if (f.exists()) {
                                System.out.println("Model Already Exist: " + currentClassifier.getClass().getSimpleName());
                            } else {
                                currentClassifier.buildClassifier(data);
                                weka.core.SerializationHelper.write(modelName, currentClassifier);

                                if (isDebug == true) {
                                    System.out.println("Finished classifier (" + (iteration + 1) + "): " + modelName);
                                }
                            }
                            completedClassifier(iteration, true);

                        } catch (Exception ex) {
                            ex.printStackTrace();
                            completedClassifier(iteration, false);
                        }
                    }
                };

                // launch this task
                m_executorPool.execute(newTask);
            } else {
                models[i].buildClassifier(data);
            }
        }

        if (m_numExecutionSlots > 1 && m_completed + m_failed < models.length) {
            block(true);
        }
    }

    /**
     * Does the actual construction of the ensemble
     *
     * @throws Exception if something goes wrong during the training process
     */
    protected synchronized void buildCVModels() throws Exception {
        if (isDebug == true) {
            System.out.println("Starting executor pool with " + m_numExecutionSlots
                    + " slots...");
        }
        startExecutorPool();
        m_completed = 0;
        m_failed = 0;

        for (int n = 0; n < numFolds; n++) {
            final Instances train = cvTrnData[n];
            final Instances test = cvTstData[n];
            for (int i = 0; i < models.length; i++) {
                if (m_numExecutionSlots > 1) {

                    final Classifier currentClassifier = models[i];
                    final int iteration = i;
                    final int fold = n;
                    final String clsName = (currentClassifier.getClass().getSimpleName() + "-" + n);
                    final String modelname = savePath + clsName + ".model";

                    Runnable newTask = new Runnable() {
                        public void run() {
                            try {
                                if (isDebug == true) {
                                    System.out.println("Training classifier (" + (iteration + 1) + "):" + clsName);
                                }
                                File f = new File(modelname);
                                if (f.exists()) {
                                    System.out.println("Model Already Exist: " + clsName);
                                } else {
                                    currentClassifier.buildClassifier(train);
                                    // serialize model
                                    weka.core.SerializationHelper.write(modelname, currentClassifier);

                                    if (isDebug == true) {
                                        System.out.println("Saved classifier (" + (iteration + 1) + "):" + modelname);
                                    }
                                }
                                completedClassifier(iteration, true);

                            } catch (Exception ex) {
                                ex.printStackTrace();
                                //completedClassifier(iteration, false);
                                completedClassifier(clsName, false);
                            }
                        }
                    };

                    // launch this task
                    m_executorPool.execute(newTask);

                } else {
                    models[i].buildClassifier(train);
                }
            }
        }

        if (m_numExecutionSlots > 1 && m_completed + m_failed < models.length) {
            block(true);
        }
    }

    /**
     * Records the completion of the training of a single classifier. Unblocks
     * if all classifiers have been trained.
     *
     * @param iteration the iteration that has completed
     * @param success whether the classifier trained successfully
     */
    protected synchronized void completedClassifier(int iteration,
            boolean success) {

        if (!success) {
            m_failed++;
            if (isDebug) {
                System.err.println("Iteration " + iteration + " failed!");
            }
        } else {
            m_completed++;
        }

        if (m_completed + m_failed == models.length) {
            if (m_failed > 0) {
                if (isDebug) {
                    System.err.println("Problem building classifiers - some iterations failed.");
                }
            }

            // have to shut the pool down or program executes as a server
            // and when running from the command line does not return to the
            // prompt
            m_executorPool.shutdown();
            block(false);
        }
    }

    /**
     * Records the completion of the training of a single classifier. Unblocks
     * if all classifiers have been trained.
     *
     * @param iteration the iteration that has completed
     * @param success whether the classifier trained successfully
     */
    protected synchronized void completedClassifier(String cls, boolean success) {

        if (!success) {
            m_failed++;
            failed.add(cls);

            if (isDebug) {
                System.err.println("Classifier: " + cls + " failed!");
            }
        } else {
            m_completed++;
        }

        if (m_completed + m_failed == models.length) {
            if (m_failed > 0) {
                if (isDebug) {
                    System.err.println("Problem building classifiers - some iterations failed.");
                }
            }

            // have to shut the pool down or program executes as a server
            // and when running from the command line does not return to the
            // prompt
            m_executorPool.shutdown();
            block(false);
        }
    }
}
